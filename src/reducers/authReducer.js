import { LOGIN, LOGOUT, LOGIN_ERROR } from '../actions/types'

const INITIAL_STATE = {
  isSignedIn: null,
  userId: null,
  name: null,
  level: null
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case LOGIN:
      return { ...state, isSignedIn: true, ...action.payload }
    case LOGIN_ERROR:
    case LOGOUT:
      return { ...state, isSignedIn: false, userId: null, name: null, level: null }
    default:
      return state
  }
}