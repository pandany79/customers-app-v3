import React, { useEffect } from 'react';
import { connect } from 'react-redux'
import { Link } from 'react-router-dom';
import { isLoggedIn, login, logout } from './../actions'
import LoginForm from './LoginForm'

const Header = (props) => {
  const { isSignedIn } = props

  useEffect(() => {
    props.isLoggedIn()
  }, [isSignedIn])

  const onLogin = formValues => {
    props.login(formValues)
  }

  const onLogout = () => {
    props.logout()
  }

  const renderAuthentication = () => {
    if (isSignedIn === null) {
      return
    } else if (!isSignedIn) {
      return (
        <LoginForm
          isSignedIn={ isSignedIn }
          onSubmit={ onLogin }
        />
      )
    }

    return (
      <button
        className="ui button red google"
        onClick={ onLogout }
      >
        Logout
      </button>
    )
  }

  return (
    <div className="ui secondary pointing menu">
      <Link
        to="/"
        className="item"
      >
        <h2>Customers App v3</h2>
      </Link>
      <div className="right menu">
        { renderAuthentication() }
      </div>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    isSignedIn: state.auth.isSignedIn
  }
}

export default connect(
  mapStateToProps,
  { isLoggedIn, login, logout }
)(Header);