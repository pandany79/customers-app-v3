import React from 'react';
import { Field, reduxForm } from 'redux-form'
import { Link } from 'react-router-dom';

const renderError = ({ touched, error }) => {
  if (touched && error) {
    return (
      <div className="ui error message">
        { error }
      </div>
    )
  }
}

const renderInput = ({ input, meta, label, placeholder, type='text' }) => {
  const className = `field ${meta.touched && meta.error ? 'error' : '' }`

  return (
    <div className={ className }>
      <label>{ label }</label>
      <input
        type={ type }
        placeholder= { placeholder }
        autoComplete="off"
        { ...input }
      />
      { renderError(meta) }
    </div>
  )
}

const CustomerForm = (props) => {
  const onSubmit = formValues => {
    props.onSubmit(formValues)
  }

  return (
    <form
      className="ui form error"
      onSubmit={ props.handleSubmit(onSubmit) }
    >
      <Field
        name="name"
        label="Name"
        placeholder="Customer's Name"
        component={ renderInput }
      />
      <Field
        type="number"
        name="age"
        label="Age"
        placeholder="Customer's Age"
        component={ renderInput }
      />
      <Field
        name="code"
        label="Code"
        placeholder="Customer's Code"
        component={ renderInput }
      />
      <button className="ui button primary">
        Submit
      </button>
      <Link
        to="/"
        className="ui button"
      >
        Cancel
      </Link>
    </form>
  );
};

const validate = formValues => {
  const errors = {}

  if (!formValues.name) {
    errors.name = `You must provide the Customer's name`
  }

  if (!formValues.age) {
    errors.age = `You must provide the Customer's age`
  } else if (isNaN(formValues.age)) {
    errors.age = 'Age must be a number'
  } else if (formValues.age < 0 || formValues.age > 120) {
    errors.age = 'Age is invalid'
  }

  if (!formValues.code) {
    errors.code = `You must provide a Customer's code`
  }

  return errors
}

export default reduxForm({
  form: 'customerForm',
  validate
})(CustomerForm);