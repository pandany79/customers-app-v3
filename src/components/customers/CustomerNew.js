import React, { useEffect } from 'react';
import { connect } from 'react-redux'
import { addCustomer } from './../../actions'
import { accessControl } from './../../helpers/accessControl'
import CustomerForm from './CustomerForm'

const CustomerNew = (props) => {
  useEffect(() => {
  }, [props.userLevel])

  const onSubmit = formValues => {
    props.addCustomer(formValues)
  }

  return (
    <div>
      <h3>Add Customer</h3>
      <CustomerForm onSubmit={ onSubmit } />
    </div>
  );
};

const mapStateToProps = state => {
  return {
    userLevel: state.auth.level
  }
}

const reduxConnect = connect(
  mapStateToProps,
  { addCustomer }
)(CustomerNew)

export default accessControl([
  'admin',
  'editor'
])(reduxConnect)