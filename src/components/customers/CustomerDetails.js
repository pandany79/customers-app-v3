import React, { useEffect, Fragment } from 'react';
import { connect } from 'react-redux'
import _ from 'lodash'
import { accessControl } from './../../helpers/accessControl'
import { getCustomer } from './../../actions'
import { Link } from 'react-router-dom';

const CustomerDetails = (props) => {
  const { id } = props.match.params

  useEffect(() => {
    props.getCustomer(id)
  }, [props.userLevel])

  if (_.isEmpty(props.customer)) {
    return (
      <Fragment>
        <h3>Customer not found!</h3>
        <Link
          to="/"
          className="ui button primary"
        >
          Back to List
        </Link>
      </Fragment>
      
    )
  }

  const { name, age, code } = props.customer
  return (
    <div>
      <h2>{ name }</h2>
      <h5>{ `Age: ${age}` }</h5>
      <h5>{ `Code: ${code}` }</h5>
      <Link
        to="/"
        className="ui button primary"
      >
        Back to List
      </Link>
    </div>
  );
};

const mapStateToProps = (state, ownProps) => {
  const { id } = ownProps.match.params
  return {
    userLevel: state.auth.level,
    customer: state.customers[id]
  }
}

const reduxConnect = connect(
  mapStateToProps,
  { getCustomer }
)(CustomerDetails)

export default accessControl([
  'admin',
  'editor',
  'user'
])(reduxConnect);