import React, { useEffect, Fragment } from 'react';
import _ from 'lodash'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom';
import { getCustomer, deleteCustomer } from './../../actions'
import { accessControl } from './../../helpers/accessControl'
import Modal from './../Modal'
import history from './../../helpers/history'

const CustomerDelete = (props) => {
  const { id } = props.match.params

  useEffect(() => {
    props.getCustomer(id)
  }, [props.userLevel])

  const onDeleteClick = () => {
    props.deleteCustomer(id)
  }

  const actions = (
    <Fragment>
      <button
        className="ui button red"
        onClick={ onDeleteClick }
      >
        <i className="trash alternate icon" />
        Confirm
      </button>
      <Link
        to="/"
        className="ui button"
      >
        Cancel
      </Link>
    </Fragment>
  )

  if (_.isEmpty(props.customer)) {
    return (
      <Fragment>
        <h3>Customer not found!</h3>
        <Link
          to="/"
          className="ui button primary"
        >
          Back to List
        </Link>
      </Fragment>
    )
  }

  const content = `Do you confirm to remove customer ${props.customer.name}?`

  return (
    <Modal
      header="Delete Customer"
      content={ content }
      actions={ actions }
      onDismiss={ () => history.pushState('/') }
    />
  );
};

const mapStateToProps = (state, ownProps) => {
  const { id } = ownProps.match.params

  return {
    userLevel: state.auth.level,
    customer: state.customers[id]
  }
}

const reduxConnect = connect(
  mapStateToProps,
  { getCustomer, deleteCustomer }
)(CustomerDelete)

export default accessControl(['admin'])(reduxConnect);