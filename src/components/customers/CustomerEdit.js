import React, { useEffect, Fragment } from 'react';
import { connect } from 'react-redux'
import { Link } from 'react-router-dom';
import _ from 'lodash'
import { getCustomer, editCustomer } from './../../actions'
import { accessControl } from './../../helpers/accessControl'
import CustomerForm from './CustomerForm'

const CustomerEdit = (props) => {
  const { id } = props.match.params

  useEffect(() => {
    props.getCustomer(id)
  }, [props.userLevel])

  const onSubmit = formValues => {
    props.editCustomer(id, formValues)
  }

  if (_.isEmpty(props.customer)) {
    return (
      <Fragment>
        <h3>Customer not found!</h3>
        <Link
          to="/"
          className="ui button primary"
        >
          Back to List
        </Link>
      </Fragment>
    )
  }

  return (
    <div>
      <h3>Edit Customer</h3>
      <div>
        <CustomerForm
          initialValues={ _.pick(props.customer, 'name', 'age', 'code') }
          onSubmit={ onSubmit }
        />
      </div>
    </div>
  );
};

const mapStateToProps = (state, ownProps) => {
  const { id } = ownProps.match.params
  return {
    userLevel: state.auth.level,
    customer: state.customers[id]
  }
}

const reduxConnect = connect(
  mapStateToProps,
  { getCustomer, editCustomer }
)(CustomerEdit)

export default accessControl([
  'admin',
  'editor'
])(reduxConnect)