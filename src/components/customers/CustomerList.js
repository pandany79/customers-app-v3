import React, { useEffect } from 'react';
import { connect } from 'react-redux'
import { accessControl } from './../../helpers/accessControl'
import { getCustomers } from './../../actions'
import { Link } from 'react-router-dom';

const CustomerList = (props) => {
  useEffect(() => {
    props.getCustomers()
  }, [props.userLevel])

  const { customers } = props

  const renderEditButton = customer => {
    if (['admin', 'editor'].includes(props.userLevel)) {
      return (
        <Link
          to={ `/customers/edit/${customer.id}` }
          className="ui button primary"
        >
          <i className="edit icon" />
          Edit
        </Link>
      )
    }
  }

  const renderDeletebutton = customer => {
    if (props.userLevel === 'admin') {
      return (
        <Link
          to={ `/customers/delete/${customer.id}` }
          className="ui button negative"
        >
          <i className="trash alternate icon" />
          Delete
        </Link>
      )
    }
  }

  const renderList = () => {
    if (!customers.length) {
      return (
        <div className="item">
          There are no registered Customers!
        </div>
      )
    }

    return customers.map(customer => {
      return (
        <div
          key={ customer.id }
          className="item"
        >
          <div className="content right floated">
            { renderEditButton(customer) }
            { renderDeletebutton(customer) }
          </div>
          <div className="content">
            <Link
              to={ `/customers/${customer.id}` }
              className="header"
            >
              { customer.name }
            </Link>
          </div>
        </div>
      )
    })
  }

  const renderButton = () => {
    if (['admin', 'editor'].includes(props.userLevel)) {
      return (
        <div style={{ textAlign: 'right' }}>
          <Link
            to="/customers/new"
            className="ui button primary"
          >
            <i className="user plus icon" />
            Add Customer
          </Link>
        </div>
      )
    }
  }

  return (
    <div>
      <h2>Customers List</h2>
      <div className="ui celled list">
        { renderList() }
      </div>
      { renderButton() }
    </div>
  );
};

const mapStateToProps = state => {
  return {
    userLevel: state.auth.level,
    customers: Object.values(state.customers)
  }
}

const reduxConnect = connect(
  mapStateToProps,
  { getCustomers }
)(CustomerList)

export default accessControl([
  'admin',
  'editor',
  'user'
])(reduxConnect);