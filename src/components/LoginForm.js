import React from 'react'
import { Field, reduxForm } from 'redux-form'

const renderInput = ({ input, meta, label, placeholder, type = 'text' }) => {
  const className = `field ${meta.touched && meta.error ? 'error' : '' }`

  return (
    <div className={ className }>
      <input
        type={ type }
        placeholder={ placeholder }
        autoComplete="off"
        { ...input }
      />
    </div>
  )
}

const LoginForm = (props) => {
  const onSubmit = formValues => {
    props.onSubmit(formValues)
    props.reset()
  }

  return (
    <form
      className="ui form error"
      onSubmit={ props.handleSubmit(onSubmit) }
    >
      <div className="fields">
        <Field
          name="username"
          placeholder="Username"
          component={ renderInput }
        />
        <Field
          name="password"
          type="password"
          placeholder="Password"
          component={ renderInput }
        />
        <button className="ui button blue google">
          Login
        </button>
      </div>
    </form>
  )
}

const validate = formValues => {
  const errors = {}

  if (!formValues.username) {
    errors.username = true
  }

  if (!formValues.password) {
    errors.password = true
  }

  return errors
}

export default reduxForm({
  form: 'loginForm',
  validate
})(LoginForm)