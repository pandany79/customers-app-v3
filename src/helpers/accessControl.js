import React, { Component } from 'react';
import { connect } from 'react-redux'

export const accessControl = levelsAllowed => WrappedComponent => {
  const SecuredControl = class extends Component {
    render() {
      // If the user is not logged in (no level defined), display a message to Login.
      if (!this.props.level) {
        return (
          <h3>Please, login!</h3>
        )
      }

      // Verify if the user is allowed to access the current component.
      const isAllowed = levelsAllowed.includes(this.props.level)

      // If the user is not allowed, display an error message.
      if (!isAllowed) {
        return (
          <h3>
            <i>You are not allowed to access this section!</i>
          </h3>
        )
      }

      return <WrappedComponent { ...this.props } />
    }
  }

  const mapStateToProps = state => {
    return {
      level: state.auth.level
    }
  }

  return connect(mapStateToProps)(SecuredControl)
}

export default accessControl;