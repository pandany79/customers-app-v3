import React from 'react';
import { Router, Route, Switch } from 'react-router-dom'
import Header from './components/Header'
import history from './helpers/history'
import CustomerList from './components/customers/CustomerList'
import CustomerDetails from './components/customers/CustomerDetails'
import CustomerNew from './components/customers/CustomerNew'
import CustomerEdit from './components/customers/CustomerEdit'
import CustomerDelete from './components/customers/CustomerDelete'

function App() {
  return (
    <Router history={ history }>
      <div className="ui container">
        <Header />
        <Switch>
          <Route path="/" exact component={ CustomerList } />
          <Route path="/customers/new" component={ CustomerNew } />
          <Route path="/customers/edit/:id" component={ CustomerEdit } />
          <Route path="/customers/delete/:id" component={ CustomerDelete } />
          <Route path="/customers/:id" component={ CustomerDetails } />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
