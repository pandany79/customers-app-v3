import {
  LOGIN,
  LOGIN_ERROR,
  LOGOUT,
  ADD_CUSTOMER,
  GET_CUSTOMERS,
  GET_CUSTOMER,
  EDIT_CUSTOMER,
  DELETE_CUSTOMER
} from './types'
import api from './../api'
import history from './../helpers/history'

export const isLoggedIn = () => async dispatch => {
  const userId = sessionStorage.getItem('userId')

  if (userId) {
    const user = (await api.get(`/users/${userId}`)).data

    dispatch({
      type: LOGIN,
      payload: {
        userId: user.id,
        name: user.name,
        level: user.level
      }
    })
  } else {
    dispatch({
      type: LOGOUT
    })
  }
}

export const login = formValues => async dispatch => {
  const { username, password } = formValues || {}
  const users = (await api.get('/users')).data
  const user = users.find(user => user.username === username && user.password === password)

  if (user) {
    const { id, name, level } = user

    await api.post('/logs', {
      userId: id,
      action: LOGIN,
      date: new Date()
    })

    sessionStorage.setItem('userId', id)

    dispatch({
      type: LOGIN,
      payload: {
        userId: id,
        name,
        level
      }
    })
  } else {
    sessionStorage.clear()

    dispatch ({
      type: LOGIN_ERROR
    })
  }
}

export const logout = () => async (dispatch, getState) => {
  const { id } = getState().auth
  sessionStorage.clear()

  await api.post('/logs', {
    userId: id,
    action: LOGOUT,
    date: new Date()
  })

  dispatch({
    type: LOGOUT
  })
}

export const addCustomer = formValues => async (dispatch, getState) => {
  const { userId } = getState().auth
  const customer = (await api.post('/customers', formValues)).data

  await api.post('/logs', {
    userId,
    action: `${ADD_CUSTOMER} ${customer.name}`,
    date: new Date()
  })

  dispatch({
    type: ADD_CUSTOMER,
    payload: customer
  })

  history.push('/')
}

export const getCustomers = () => async dispatch => {
  const customers = (await api.get('/customers')).data

  dispatch({
    type: GET_CUSTOMERS,
    payload: customers
  })
}

export const getCustomer = id => async dispatch => {
  const customer = (await api.get(`/customers/${id}`)).data

  dispatch({
    type: GET_CUSTOMER,
    payload: customer
  })
}

export const editCustomer = (id, formValues) => async (dispatch, getState) => {
  const { userId } = getState().auth
  const customer = (await api.put(`/customers/${id}`, formValues)).data

  await api.post('/logs', {
    userId,
    action: `${EDIT_CUSTOMER} ${customer.name}`,
    date: new Date()
  })

  dispatch({
    type: EDIT_CUSTOMER,
    payload: customer
  })

  history.push('/')
}

export const deleteCustomer = id => async (dispatch, getState) => {
  const { userId } = getState().auth
  const customer = (await api.get(`/customers/${id}`)).data

  await api.delete(`/customers/${id}`)
  await api.post('/logs', {
    userId,
    action: `${DELETE_CUSTOMER} ${customer.name}`,
    date: new Date()
  })

  dispatch({
    type: DELETE_CUSTOMER,
    payload: id
  })

  history.push('/')
}